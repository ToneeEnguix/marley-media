import React, { useState, useEffect, useContext } from "react";
import "./App.css";
import { __RouterContext } from "react-router";
import { Switch, Route } from "react-router-dom";
import Landing from "./Components/Landing/Landing";
import Menu from "./Components/Menu/Menu";
import Legal from "./Components/Legal/Legal";
import About from "./Components/About/About";
import Type from "./Components/Type/Type";
import Domain from "./Components/Domain/Domain";
import NoDomain from "./Components/NoDomain/NoDomain";
import Branding from "./Components/Branding/Branding";
import Photography from "./Components/Photography/Photography";
import Google from "./Components/Google/Google";
import Tour from "./Components/Tour/Tour";
import Design1 from "./Components/Design1/Design1";
import Design2 from "./Components/Design2/Design2";
import Business from "./Components/Business/Business";
import Last from "./Components/Last/Last";
import Service from "./Components/Service/Service";
import Successful from "./Components/Successful/Successful";
import Unsuccessful from "./Components/Unsuccessful/Unsuccessful";
import Project from "./Components/Project/Project";
import { useTransition, animated } from "react-spring";
import emailjs from "emailjs-com";

const App = () => {
  const [info, setInfo] = useState({});
  const { location } = useContext(__RouterContext);
  const transitions = useTransition(location, (location) => location.pathname, {
    from: { transform: "translate(100%, 0)" },
    enter: { transform: "translate(0, 0)" },
    leave: { transform: "translate(-100%, 0)" },
  });

  useEffect(() => {
    console.log("info: ", info);
  }, [info]);

  const sendMail = (key) => {
    console.log(
      "kiwi",
      info.type,
      info.domain,
      info.nodomain,
      info.branding,
      info.photography,
      info.google,
      info.virtualTour,
      info.design1,
      info.design2,
      info.business,
      info.final,
      key
    );
    if (
      info.type &&
      (info.domain || info.nodomain) &&
      info.branding &&
      info.photography &&
      info.google &&
      info.virtualTour &&
      info.design1 &&
      info.design2 &&
      info.business &&
      key === true
    ) {
      console.log("banana");
      emailjs.send(
        "service_xh5dwua",
        "template_osvlo3i",
        {
          subject: `New proposal by ${info.business.company_name}`,
          body: `Application type: ${info.type.cost.slice(4)}, ${`discount: ${info.type.code || "none"}`
          }, ${info.domain && info.domain.name} `,
        },
        "user_abl4QqAVHwzvFneUdLTl1"
      );
    }
  };

  return (
    <>
      {transitions.map(({ item, props, key }) => (
        <animated.div key={key} style={props}>
          <Switch location={item}>
            <Route exact path="/" component={Landing} />
            <Route exact path="/about" component={About} />
            <Route
              exact
              path="/type"
              render={(props) => (
                <Type
                  {...props}
                  info={info}
                  setInfo={(type) => setInfo({ ...info, type })}
                />
              )}
            />
            {/* <Route exact path="/type" component={Type} /> */}
            <Route exact path="/menu" component={Menu} />
            <Route exact path="/project" component={Project} />
            <Route exact path="/legal" component={Legal} />
            <Route
              exact
              path="/domainname"
              render={(props) => (
                <Domain
                  {...props}
                  info={info}
                  setInfo={(domain) => setInfo({ ...info, domain })}
                />
              )}
            />
            <Route
              exact
              path="/nodomain"
              render={(props) => (
                <NoDomain
                  {...props}
                  info={info}
                  setInfo={(nodomain) => setInfo({ ...info, nodomain })}
                />
              )}
            />
            <Route
              exact
              path="/branding"
              render={(props) => (
                <Branding
                  {...props}
                  info={info}
                  setInfo={(branding) => setInfo({ ...info, branding })}
                />
              )}
            />
            <Route
              exact
              path="/photography"
              render={(props) => (
                <Photography
                  {...props}
                  info={info}
                  setInfo={(photography) => setInfo({ ...info, photography })}
                />
              )}
            />
            <Route
              exact
              path="/googlemybusiness"
              render={(props) => (
                <Google
                  {...props}
                  info={info}
                  setInfo={(google) => setInfo({ ...info, google })}
                />
              )}
            />
            <Route
              exact
              path="/virtualtour"
              render={(props) => (
                <Tour
                  {...props}
                  info={info}
                  setInfo={(virtualTour) => setInfo({ ...info, virtualTour })}
                />
              )}
            />
            <Route
              exact
              path="/designone"
              render={(props) => (
                <Design1
                  {...props}
                  info={info}
                  setInfo={(design1) => setInfo({ ...info, design1 })}
                />
              )}
            />
            <Route
              exact
              path="/designtwo"
              render={(props) => (
                <Design2
                  {...props}
                  info={info}
                  setInfo={(design2) => setInfo({ ...info, design2 })}
                />
              )}
            />
            <Route
              exact
              path="/businessdetail"
              render={(props) => (
                <Business
                  {...props}
                  info={info}
                  setInfo={(business) => setInfo({ ...info, business })}
                />
              )}
            />
            <Route
              exact
              path="/final"
              render={(props) => (
                <Last
                  {...props}
                  info={info}
                  setInfo={(final) => setInfo({ ...info, final })}
                />
              )}
            />
            <Route
              exact
              path="/agreement"
              render={(props) => (
                <Service {...props} info={info} sendMail={sendMail} />
              )}
            />
            <Route exact path="/unsuccessful" component={Unsuccessful} />
            <Route
              exact
              path="/successful"
              render={(props) => (
                <Successful {...props} info={info} sendMail={sendMail} />
              )}
            />
            <Route exact path="/unsuccessful" component={Unsuccessful} />
          </Switch>
        </animated.div>
      ))}
    </>
  );
};

export default App;

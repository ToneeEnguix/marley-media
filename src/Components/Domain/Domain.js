import React, { useEffect, useState } from "react";
import "./Domain.css";
import "../../App.css";
import arrowBlack from "../../Images/arrowBlack.svg";
import { Link, useLocation } from "react-router-dom";

const Domain = (props) => {
  const { pathname } = useLocation();
  const [URLmessage, setURLmessage] = useState("");
  const [domain, setDomain] = useState({
    hasName: "",
    name: "",
    company: "",
    consent: "",
    username: "",
    password: "",
    password2: "",
  });

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  useEffect(() => {
    let timer;
    if (URLmessage !== "") {
      window.clearTimeout(timer);
      timer = window.setTimeout(function () {
        setURLmessage("");
      }, 2000);
    }
  }, [URLmessage]);

  const validURL = (str) => {
    str = str.toLowerCase();
    const regexp = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
    if (regexp.test(str)) {
      return true;
    } else {
      return false;
    }
  };

  const sendInfo = () => {
    if (validURL(domain.name) !== true) {
      return setURLmessage(
        'Website name must be a URL. Go to your site and "Copy + Paste" it!'
      );
    }
    if (domain.company === "") {
      return setURLmessage(
        "Please fill the company name that you used to register your website"
      );
    }
    if (domain.consent) {
      if (
        domain.username === "" ||
        domain.password === "" ||
        domain.password !== domain.password2
      ) {
        return setURLmessage(
          "Please write your Hosting provider username and password. Make sure they match!"
        );
      }
    }
    props.setInfo(domain);
  };

  return (
    <div className="section page">
      <p className="marley bold">marley media</p>
      <div className="d_skipContainer">
        <p className="subtitle light d_pre nanum">
          Your Domain name. If you don't have a domain name,
        </p>
        <Link to="nodomain" className="linkBlack">
          <p className="underline subtitle pointer light">skip</p>
        </Link>
      </div>
      <div className="d_tripleCont">
        <div className="checkboxContHalf">
          <p
            className="d_input d_input1 pointer nanum"
            onClick={() => setDomain({ ...domain, hasName: !domain.hasName })}
          >
            I have registered a domain name.
          </p>
          <div
            className="outside pointer"
            onClick={() => setDomain({ ...domain, hasName: !domain.hasName })}
          >
            <div className={`inside${domain.hasName} pointer`}></div>
          </div>
        </div>
        <input
          placeholder="My domain name is | www."
          className={`inputHalf ${!domain.hasName && "nope"}`}
          readOnly={!domain.hasName}
          onChange={(e) => setDomain({ ...domain, name: e.target.value })}
        />
      </div>
      <input
        placeholder="The company name is:"
        className={`inputFull ${!domain.hasName && "nope"}`}
        readOnly={!domain.hasName}
        onChange={(e) => setDomain({ ...domain, company: e.target.value })}
      />
      <div
        className="checkboxCont"
        onClick={() =>
          domain.hasName && setDomain({ ...domain, consent: !domain.consent })
        }
      >
        <p
          className={`inputAlmost nanum ${
            !domain.hasName ? "nope" : "pointer"
          }`}
        >
          I give my consent for my domain name servers to point toward Nebula
          Servers.
        </p>
        <div
          className={`outside pointer ${!domain.hasName ? "nope" : "pointer"}`}
        >
          <div
            className={`inside${domain.consent} ${
              !domain.hasName ? "nope" : "pointer"
            }`}
          ></div>
        </div>
      </div>
      <input
        placeholder="Provide username of my account for my current Hosting Provider."
        className={`inputFull ${
          (!domain.hasName || !domain.consent) && "nope"
        }`}
        onChange={(e) => setDomain({ ...domain, username: e.target.value })}
        readOnly={!domain.hasName || !domain.consent}
      />
      <div className="inputContainer">
        <input
          placeholder="Provide password of my account for my current Hosting Provider."
          className={`inputHalf ${
            (!domain.hasName || !domain.consent) && "nope"
          }`}
          onChange={(e) => setDomain({ ...domain, password: e.target.value })}
          readOnly={!domain.hasName || !domain.consent}
          type="password"
        />
        <input
          placeholder="Repeat password."
          className={`inputHalf ${
            (!domain.hasName || !domain.consent) && "nope"
          }`}
          onChange={(e) => setDomain({ ...domain, password2: e.target.value })}
          readOnly={!domain.hasName || !domain.consent}
          type="password"
        />
      </div>
      <Link
        to={`${
          domain.consent === true
            ? domain.username !== "" &&
              domain.password !== "" &&
              validURL(domain.name) &&
              domain.company !== "" &&
              domain.password === domain.password2
              ? "/branding"
              : "/domainname"
            : validURL(domain.name) && domain.company !== ""
            ? "/branding"
            : "/domainname"
        }`}
      >
        <img
          src={arrowBlack}
          className={`arrow pointer ${!domain.hasName && "nope"}`}
          alt="arrow"
          onClick={() => domain.hasName && sendInfo()}
        />
      </Link>
      {URLmessage !== "" && <p className="white message">{URLmessage}</p>}
    </div>
  );
};

export default Domain;

import React, { useEffect, useState } from "react";
import "./Google.css";
import "../../App.css";
import arrowBlack from "../../Images/arrowBlack.svg";
import { Link, useLocation } from "react-router-dom";

const NoDomain = (props) => {
  const { pathname } = useLocation();
  const [understandingMessage, setUnderstandingMessage] = useState("");
  const [google, setGoogle] = useState({
    has_google_account: true,
    needs_google_account: false,
    understanding: false,
  });

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  useEffect(() => {
    let timer;
    if (understandingMessage !== "") {
      window.clearTimeout(timer);
      timer = window.setTimeout(function () {
        setUnderstandingMessage("");
      }, 2000);
    }
  }, [understandingMessage]);

  const sendInfo = () => {
    if (google.needs_google_account && !google.understanding) {
      return setUnderstandingMessage(
        "In order to manage your Google My Business account we need the validation pin code."
      );
    }
    props.setInfo(google);
  };

  return (
    <div className="section page">
      <p className="marley bold">marley media</p>
      <p className="subtitle light">Google My Business</p>
      <div
        className="checkboxCont"
        onClick={() =>
          setGoogle({
            ...google,
            has_google_account: !google.has_google_account,
            needs_google_account: !google.needs_google_account,
          })
        }
      >
        <p className="inputAlmost pointer nanum">
          I have a Google My Business account set up already.
        </p>
        <div className="outside pointer">
          <div className={`inside${google.has_google_account} pointer`}></div>
        </div>
      </div>
      <div
        className="checkboxCont"
        onClick={() =>
          setGoogle({
            ...google,
            needs_google_account: !google.needs_google_account,
            has_google_account: !google.has_google_account,
            understanding: true && false,
          })
        }
      >
        <p className="inputAlmost pointer nanum">
          I want you to manage my Google My Business account. Add £30
        </p>
        <div className="outside pointer">
          <div className={`inside${google.needs_google_account} pointer`}></div>
        </div>
      </div>
      <div
        className="checkboxCont"
        onClick={() =>
          google.needs_google_account &&
          setGoogle({
            ...google,
            understanding: !google.understanding,
          })
        }
      >
        <p
          className={`inputAlmost nanum ${
            google.needs_google_account ? "pointer" : "nope"
          }`}
        >
          I understand that I need to provide Nebula Industries with the
          validation pin code from the Google My Business postcard.
        </p>
        <div
          className={`outside ${
            google.needs_google_account ? "pointer" : "nope"
          }`}
        >
          <div className={`inside${google.understanding} pointer`}></div>
        </div>
      </div>
      <Link
        to={`${
          google.has_google_account
            ? "/virtualtour"
            : google.needs_google_account && google.understanding
            ? "/virtualtour"
            : "/googlemybusiness"
        }`}
      >
        <img
          src={arrowBlack}
          className="arrow pointer"
          alt="arrow"
          onClick={() => sendInfo()}
        />
      </Link>
      {understandingMessage !== "" && (
        <p className="white message">{understandingMessage}</p>
      )}
    </div>
  );
};

export default NoDomain;

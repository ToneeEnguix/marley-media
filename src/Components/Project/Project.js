import React, { useEffect } from "react";
import { useLocation, Link } from "react-router-dom";
import "./Project.css";
import "../Menu/menu.css";
import "../../App.css";
import arrowWhite from "../../Images/arrowWhite.svg";
import Menu from "../Menu/Menu";

const Project = (props) => {
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return (
    <div className="project page">
      <div className="p_navbar">
        <div>
          <Link className="linkBlack" to="/">
            <p className="marley p_marleyNavbar pointer">marley media</p>
          </Link>
        </div>
        <div className="p_navbarRight">
          <Link to="/type" className="linkBlack">
            <p className="light nanum p_submit">Submit your application</p>
          </Link>
          <a href="tel:07979054325">
            <p className="p_number damascus thisgray">079790 54325</p>
          </a>
        </div>
      </div>
      <div className="p_jpgCont">
        <img
          src="https://www.putteringaroundtheworld.com/wp-content/uploads/2019/07/99-West-1-1024x768.jpg"
          className="p_jpg"
          alt="project cover"
        />
      </div>
      <h3 className="light nanum p_intro">Title intro</h3>
      <h1 className="light nanumSerif p_title">This is a wonderful title</h1>
      <h1 className="light nanumSerif p_title">
        To an excellent project we have completed
      </h1>
      <p className="p_p nanum light">
        Our first engagement with Clarasys was to re-build their existing
        website like-for-like. Their existing website was slow and the on-page
        front-end code was poor quality, resulting in lower Google positions
        than they could otherwise achieve. Our rebuild approach quickly overcame
        these issues, enabling their website to successfully compete for
        valuable Google rankings.
      </p>
      <p className="p_p p_p1 nanum light">
        Initial results from this re-code was a 77% improvement in website
        speed, and a jump in their organic rankings resulting in a 15% increase
        in target market visitors.
      </p>
      <div className="p_items">
        <div className="thisgray p_itemsL">
          <p className="p_itemL light">Lead Time:</p>
          <p className="p_itemL light">Sector:</p>
          <p className="p_itemL light">Target Type:</p>
          <p className="p_itemL light">Demographic:</p>
          <p className="p_itemL light">Website Goal:</p>
          <p className="p_itemL light">Services:</p>
        </div>
        <div className="p_itemsR">
          <p className="p_itemR light">8 weeks</p>
          <p className="p_itemR light">B2B Consultants</p>
          <p className="p_itemR light">B2B</p>
          <p className="p_itemR light">
            Directors and Executives in large organisations
          </p>
          <p className="p_itemR light">Increase lead generation</p>
          <p className="p_itemR light">
            Web Design, Web Development, Digital Strategy, SEO
          </p>
        </div>
      </div>
      <Link to="/type">
        <button className="p_btn white nanum light pointer">
          Start your project
          <img src={arrowWhite} className="p_arrow" alt="arrow" />
        </button>
      </Link>
      <div className="p_menu">
        <div className="m_content">
          <div className="m_flex">
            <div className="m_grid6">
              <div>
                <p className="marley bold">marley media</p>
                <p className="thisgray m_littletext bold">
                  We create possibilities
                </p>
                <p className="thisgray m_littletext bold">
                  for the connected world.
                </p>
                <p className="bold m_littletext">Be Bold.</p>
              </div>
              <address className="m_column">
                <p className="m_littletitle bold">Explore</p>
                <Link to="/">
                  <p className="thisgray m_littletext bold pointer">Home</p>
                </Link>
                <Link to="/about">
                  <p className="thisgray m_littletext bold pointer">About</p>
                </Link>
                <p className="thisgray m_littletext bold">Capabilities</p>
                <p className="thisgray m_littletext bold">Careers</p>
              </address>
              <address className="m_column">
                <a
                  href="https://www.google.com/maps/place/25+High+St,+Belfast+BT1+1WL,+UK/@54.5995869,-5.9297645,17z/data=!3m1!4b1!4m8!1m2!2m1!1sHigh+St,+Belfast+UK++25,+Highbridge+House!3m4!1s0x486108569d3826eb:0x5debc7fcfe88ed84!8m2!3d54.5995838!4d-5.9275758"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <p className="m_littletitle bold pointer white">
                    Visit Belfast
                  </p>
                  <p className="thisgray m_littletext bold pointer">
                    High St, Belfast UK
                  </p>
                  <p className="thisgray m_littletext bold pointer">
                    25, Highbridge House
                  </p>
                </a>
                <p className="m_littletitle bold m_business">Contact Belfast</p>
                <a href="mailto:engage@weareenvoy.com">
                  <p className="thisgray m_littletext bold pointer">
                    engage@weareenvoy.com
                  </p>
                </a>
                <a href="tel:9493333106">
                  <p className="thisgray m_littletext bold">949.333.3106</p>
                </a>
              </address>
              <div className="m_column">
                <a
                  href="https://www.google.com/maps/place/Carrer+de+Balmes,+94,+08008+Barcelona/@41.3916009,2.1578394,17z/data=!3m1!4b1!4m5!3m4!1s0x12a4a29202fdddfd:0xb71dfa785260ce10!8m2!3d41.3915969!4d2.1600281"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <p className="m_littletitle bold white">Visit Barcelona</p>
                  <p className="thisgray m_littletext bold">
                    Carrer de Balmes, 94
                  </p>
                  <p className="thisgray m_littletext bold">08008 Barcelona</p>
                </a>
                <p className="m_littletitle bold m_business">
                  Contact Barcelona
                </p>
                <a href="tel:9493333106">
                  <p className="thisgray m_littletext bold">949.333.3106</p>
                </a>
              </div>
              <div className="m_column">
                <p className="m_littletitle bold">Follow</p>
                <p className="thisgray m_littletext bold">Instagram</p>
                <p className="thisgray m_littletext bold">Twitter</p>
                <p className="thisgray m_littletext bold">LinkedIn</p>
              </div>
              <div className="m_column">
                <p className="m_littletitle bold">Legal</p>
                <Link to="/legal">
                  <p className="thisgray m_littletext bold pointer">Terms</p>
                </Link>
                <Link to="/legal">
                  <p className="thisgray m_littletext bold pointer">Privacy</p>
                </Link>
              </div>
            </div>
          </div>
          <p className="thisgray m_littletext bold">
            © 2020 Envoy. All rights reserved.
          </p>
        </div>
      </div>
      <img
        src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/4f/Simple_triangle.svg/768px-Simple_triangle.svg.png"
        className="triangle"
        alt="triangle"
      />
      <div className="circle"></div>
      <div className="p_menu2">
        <Menu />
      </div>
    </div>
  );
};

export default Project;

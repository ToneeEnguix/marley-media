import React, { useEffect, useState } from "react";
import "./Last.css";
import "../../App.css";
import arrowBlack from "../../Images/arrowBlack.svg";
import { Link, useLocation } from "react-router-dom";

const Last = (props) => {
  const { pathname } = useLocation();
  const [last, setLast] = useState({
    products_services: "",
    team_members_positions: "",
    testimonials: "",
  });

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  const sendInfo = () => {
    props.setInfo(last);
  };

  return (
    <div className="section page">
      <p className="marley bold">marley media</p>
      <p className="subtitle light">Last Part, Don't worry!</p>
      <textarea
        placeholder="Provide a list of your products and Services if applicable."
        className="l_input nanum"
        onChange={(e) =>
          setLast({ ...last, products_services: e.target.value })
        }
      />
      <textarea
        placeholder="Please provide the names of your teams members and their positions."
        className="l_input nanum"
        onChange={(e) =>
          setLast({ ...last, team_members_positions: e.target.value })
        }
      />
      <textarea
        placeholder="Please enter testimonial(s) and their authors you wish to use in your website if applicable."
        className="l_input nanum"
        onChange={(e) => setLast({ ...last, testimonials: e.target.value })}
      />
      <Link to="/agreement">
        <img
          src={arrowBlack}
          className="arrow pointer"
          alt="arrow"
          onClick={() => sendInfo()}
        />
      </Link>
    </div>
  );
};

export default Last;

import React, { useEffect } from "react";
import "./Legal.css";
import "../../App.css";
import arrowWhite from "../../Images/arrowWhite.png";
import { Link, useLocation } from "react-router-dom";

const Legal = (props) => {
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return (
    <div className="legal page">
      <Link to="/menu">
        <img
          src={arrowWhite}
          className="menu_arrow pointer"
          alt="arrow"
        />
      </Link>
      <div>
        <p className="bold marley">marley media</p>
        <p className="thisgray l_littletext bold">Legal</p>
        <p className="little littleMenuText">
          Nebula Industries Limited is a limited company incorporated into
          Companies House in 2020. All rights reserved. 2020.
        </p>
      </div>
    </div>
  );
};

export default Legal;

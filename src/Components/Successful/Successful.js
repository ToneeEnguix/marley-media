import React, { useState, useEffect } from "react";
import "./Successful.css";
import "../../App.css";
import { useHistory } from "react-router-dom";

const Successful = () => {
  const [message, setMessage] = useState("");
  const history = useHistory();

  useEffect(() => {
    setInterval(() => {
      setMessage("Redirecting you back home...");
      setInterval(() => {
        history.push("/");
      }, 1000);
    }, 1600);
  });

  return (
    <div className="successful page">
      <p className="ss_p thisgray nanum light">Congratulations</p>
      <p className="ss_p1 white nanumSerif">
        You have successfully submitted your application.
      </p>
      <p className="ss_p2 white nanumSerif light">
        One of our agents will be in contact with you shortly.
      </p>
      <p className="thisgray light nanum ss_message">{message}</p>
    </div>
  );
};

export default Successful;

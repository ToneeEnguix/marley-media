import React, { useEffect, useState } from "react";
import "./Design2.css";
import "../../App.css";
import arrowBlack from "../../Images/arrowBlack.svg";
import { Link, useLocation } from "react-router-dom";

const NoDomain = (props) => {
  const { pathname } = useLocation();
  const [minSelected, setMinSelected] = useState("");
  const [length, setLength] = useState("");
  const [red, setRed] = useState(["home"]);
  const [blue, setBlue] = useState([]);
  const [designTwo, setDesignTwo] = useState({
    howwouldyoulike: "",
    home: true,
    about: false,
    team: false,
    services: false,
    portfolio: false,
    shop: false,
    menu: false,
    footer: false,
    extraContent: blue,
    others: [],
  });

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  useEffect(() => {
    let timer;
    if (minSelected !== "") {
      window.clearTimeout(timer);
      timer = window.setTimeout(function () {
        setMinSelected("");
      }, 2000);
    }
  }, [minSelected]);

  const changeState = (key) => {
    if (red.length === 5) {
      if (red.includes(key)) {
        if (blue.length > 0) {
          let tempRed = red;
          tempRed.splice(tempRed.indexOf(key), 1);
          let tempBlue = blue;
          let topush = tempBlue.shift();
          tempRed.push(topush);
          setRed(tempRed);
          setBlue(tempBlue);
        } else {
          let tempRed = red;
          tempRed.splice(tempRed.indexOf(key), 1);
          setRed(tempRed);
        }
      } else {
        let tempBlue = blue;
        if (tempBlue.includes(key)) {
          tempBlue.splice(tempBlue.indexOf(key), 1);
          setBlue(tempBlue);
        } else {
          tempBlue.push(key);
          setBlue(tempBlue);
        }
      }
    } else {
      let tempRed = red;
      if (tempRed.includes(key)) {
        tempRed.splice(tempRed.indexOf(key), 1);
        setRed(tempRed);
      } else {
        tempRed.push(key);
        setRed(tempRed);
      }
    }
    setDesignTwo({ ...designTwo, [key]: !designTwo[key] });
  };

  const handleOthers = (key, other) => {
    let tempOthers = designTwo.others;
    tempOthers[key] = other;
    setDesignTwo({ ...designTwo, others: tempOthers });
  };

  useEffect(() => {
    setLength(Object.values(designTwo).filter((ele) => ele).length);
  }, [designTwo]);

  const sendInfo = () => {
    if (length < 5) {
      setMinSelected("Please, select at least 5 options");
    } else {
      props.setInfo(designTwo);
    }
  };

  return (
    <div className="section page">
      <p className="marley bold">marley media</p>
      <p className="subtitle light">Design Part II</p>
      <input
        className="inputFull default nanum"
        placeholder="How would you like your customers to see your business?"
        onChange={(e) =>
          setDesignTwo({ ...designTwo, howwouldyoulike: e.target.value })
        }
      />

      <div className="d_container">
        <div className="d_container2">
          <div className="inputAlmost">
            <div className="d2_flex">
              <p className="little light">
                You get 5 sections. Add £50 per section after.
              </p>
              <div className="d2_flex2">
                <pre className="underline little light nanum">Blue dots</pre>
                <pre className="little light nanum"> indicate extra pages.</pre>
              </div>
            </div>

            <p
              className={`d_section ${
                designTwo.home ? "black" : "thisgray"
              } light pointer`}
              onClick={() => changeState("home")}
            >
              Home Page
            </p>
            <p
              className={`d_section ${
                designTwo.about ? "black" : "thisgray"
              } light pointer`}
              onClick={() => changeState("about")}
            >
              About Section
            </p>
            <p
              className={`d_section ${
                designTwo.team ? "black" : "thisgray"
              } light pointer`}
              onClick={() => changeState("team")}
            >
              Team Section
            </p>
            <p
              className={`d_section ${
                designTwo.services ? "black" : "thisgray"
              } light pointer`}
              onClick={() => changeState("services")}
            >
              Services
            </p>
            <p
              className={`d_section ${
                designTwo.portfolio ? "black" : "thisgray"
              } light pointer`}
              onClick={() => changeState("portfolio")}
            >
              Portfolio
            </p>
            <p
              className={`d_section ${
                designTwo.shop ? "black" : "thisgray"
              } light pointer`}
              onClick={() => changeState("shop")}
            >
              Shop
            </p>
            <p
              className={`d_section ${
                designTwo.menu ? "black" : "thisgray"
              } light pointer`}
              onClick={() => changeState("menu")}
            >
              Menu
            </p>
            <p
              className={`d_section d_sectionLast ${
                designTwo.footer ? "black" : "thisgray"
              } light pointer`}
              onClick={() => changeState("footer")}
            >
              Contact Footer
            </p>
          </div>
          <div className="d_checkboxes">
            <div
              className="outsideLil pointer"
              onClick={() => changeState("home")}
            >
              <div
                className={`inside${designTwo.home}Lil ${
                  blue.includes("home") && "d2_blue"
                } pointer`}
              ></div>
            </div>
            <div
              className="outsideLil pointer"
              onClick={() => changeState("about")}
            >
              <div
                className={`inside${designTwo.about}Lil ${
                  blue.includes("about") && "d2_blue"
                } pointer`}
              ></div>
            </div>
            <div
              className="outsideLil pointer"
              onClick={() => changeState("team")}
            >
              <div
                className={`inside${designTwo.team}Lil ${
                  blue.includes("team") && "d2_blue"
                } pointer`}
              ></div>
            </div>
            <div
              className="outsideLil pointer"
              onClick={() => changeState("services")}
            >
              <div
                className={`inside${designTwo.services}Lil ${
                  blue.includes("services") && "d2_blue"
                } pointer`}
              ></div>
            </div>
            <div
              className="outsideLil pointer"
              onClick={() => changeState("portfolio")}
            >
              <div
                className={`inside${designTwo.portfolio}Lil ${
                  blue.includes("portfolio") && "d2_blue"
                } pointer`}
              ></div>
            </div>
            <div
              className="outsideLil pointer"
              onClick={() => changeState("shop")}
            >
              <div
                className={`inside${designTwo.shop}Lil ${
                  blue.includes("shop") && "d2_blue"
                } pointer`}
              ></div>
            </div>
            <div
              className="outsideLil pointer"
              onClick={() => changeState("menu")}
            >
              <div
                className={`inside${designTwo.menu}Lil ${
                  blue.includes("menu") && "d2_blue"
                } pointer`}
              ></div>
            </div>
            <div
              className="outsideLil pointer"
              onClick={() => changeState("footer")}
            >
              <div
                className={`inside${designTwo.footer}Lil ${
                  blue.includes("footer") && "d2_blue"
                } pointer`}
              ></div>
            </div>
          </div>
        </div>
        <div className="inputFull">
          <p className="light little d_list">
            Please list any more pages / sections you have in mind.
          </p>
          <input
            className="d2_input1 nanum"
            onChange={(e) => handleOthers(0, e.target.value)}
          />
          <input
            className="d2_input1 nanum"
            onChange={(e) => handleOthers(1, e.target.value)}
          />
          <input
            className="d2_input1 nanum"
            onChange={(e) => handleOthers(2, e.target.value)}
          />
          <input
            className="d2_input1 nanum"
            onChange={(e) => handleOthers(3, e.target.value)}
          />
          <input
            className="d2_input1 nanum"
            onChange={(e) => handleOthers(4, e.target.value)}
          />
          <input
            className="d2_input1 nanum"
            onChange={(e) => handleOthers(5, e.target.value)}
          />
          <input
            className="d2_input1 nanum"
            onChange={(e) => handleOthers(6, e.target.value)}
          />
          <input
            className="d2_input1 nanum"
            onChange={(e) => handleOthers(7, e.target.value)}
          />
        </div>
      </div>
      <Link to={`${length < 5 ? "designtwo" : "/businessdetail"}`}>
        <img
          src={arrowBlack}
          className="arrow pointer"
          alt="arrow"
          onClick={() => sendInfo()}
        />
      </Link>
      {minSelected !== "" && <p className="white message">{minSelected}</p>}
    </div>
  );
};

export default NoDomain;

import React, { useEffect } from "react";
import "./About.css";
import "../../App.css";
import arrowWhite from "../../Images/arrowWhite.png";
import { Link, useLocation } from "react-router-dom";

const About = (props) => {
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return (
    <div className="about page">
      <Link to="/menu">
        <img src={arrowWhite} className="menu_arrow pointer" alt="arrow" />
      </Link>
      <div>
        <p className="bold marley">marley media</p>
        <p className="thisgray a_littletext bold">About</p>
        <p className="a_littletitle littleMenuText">
          We create powerful websites and apps for small to medium sized
          companies. Nebula Industries Ltd. specialise in design thinking
        </p>
        <p className="a_littletitle littleMenuText">
          Backend Development with Node, React.JS, Express, MongoDB, and mobile
          development with React Native.
        </p>
      </div>
    </div>
  );
};

export default About;

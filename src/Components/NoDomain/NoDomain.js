import React, { useEffect, useState } from "react";
import "./NoDomain.css";
import "../../App.css";
import arrowBlack from "../../Images/arrowBlack.svg";
import { Link, useLocation } from "react-router-dom";

const NoDomain = (props) => {
  const { pathname } = useLocation();
  const [preferenceMessage, setPreferenceMessage] = useState("");
  const [noDomain, setNoDomain] = useState({
    purchase_domain: true,
    three_year_subscription: true,
    one_year_subscription: false,
    domainPref1: "",
    domainPref2: "",
    domainPref3: "",
  });

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  useEffect(() => {
    let timer;
    if (preferenceMessage !== "") {
      window.clearTimeout(timer);
      timer = window.setTimeout(function () {
        setPreferenceMessage("");
      }, 2000);
    }
  }, [preferenceMessage]);

  const sendInfo = () => {
    noDomain.domainPref1 === "" &&
    noDomain.domainPref2 === "" &&
    noDomain.domainPref3 === ""
      ? setPreferenceMessage(
          "Please, write at least one domain name preference."
        )
      : props.setInfo(noDomain);
  };

  return (
    <div className="section page">
      <p className="marley bold">marley media</p>
      <div className="nd_skipContainer">
        <p className="subtitle light">No Domain name.</p>
        <pre className="nd_pre"> </pre>
        <Link to="/domainname" className="linkBlack">
          <p className="underline subtitle pointer light">Skip</p>
        </Link>
        <pre className="nd_pre"> </pre>
        <p className="subtitle light">if you have one.</p>
      </div>
      <div
        className="checkboxCont"
        onClick={() =>
          setNoDomain({
            ...noDomain,
            purchase_domain: !noDomain.purchase_domain,
            three_year_subscription: noDomain.three_year_subscription && false,
            one_year_subscription: noDomain.one_year_subscription && false,
          })
        }
      >
        <p className="inputAlmost pointer nanum">
          I do not have a domain name and I wish Nebula Industries Ltd. to
          purchase a domain name on my behalf.
        </p>
        <div className="outside pointer">
          <div className={`inside${noDomain.purchase_domain} pointer`}></div>
        </div>
      </div>
      <div
        className="checkboxCont"
        onClick={() =>
          noDomain.purchase_domain &&
          setNoDomain({
            ...noDomain,
            three_year_subscription: !noDomain.three_year_subscription,
            one_year_subscription: true && false,
          })
        }
      >
        <p
          className={`inputAlmost pointer nanum ${
            !noDomain.purchase_domain && "nope"
          }`}
        >
          .com 3 year subscription. To be renewed every three years. Add £50.
        </p>
        <div
          className={`outside ${noDomain.purchase_domain ? "pointer" : "nope"}`}
        >
          <div className={`inside${noDomain.three_year_subscription}`}></div>
        </div>
      </div>
      <div
        className="checkboxCont"
        onClick={() =>
          noDomain.purchase_domain &&
          setNoDomain({
            ...noDomain,
            one_year_subscription: !noDomain.one_year_subscription,
            three_year_subscription: true && false,
          })
        }
      >
        <p
          className={`inputAlmost nanum ${
            noDomain.purchase_domain ? "pointer" : "nope"
          }`}
        >
          .com 1 year subscription. To be renewed every year. Add £25.
        </p>
        <div
          className={`outside ${noDomain.purchase_domain ? "pointer" : "nope"}`}
        >
          <div className={`inside${noDomain.one_year_subscription}`}></div>
        </div>
      </div>
      <p className="nd_subtitle">Domain name preferences</p>
      <input
        placeholder="www."
        className="inputFull"
        value={noDomain.domainPref1}
        onChange={(e) =>
          setNoDomain({ ...noDomain, domainPref1: e.target.value })
        }
        onClick={() => setNoDomain({ ...noDomain, domainPref1: "www." })}
      />
      <input
        placeholder="www."
        className="inputFull"
        value={noDomain.domainPref2}
        onChange={(e) =>
          setNoDomain({ ...noDomain, domainPref2: e.target.value })
        }
        onClick={() => setNoDomain({ ...noDomain, domainPref2: "www." })}
      />
      <input
        placeholder="www."
        className="inputFull"
        value={noDomain.domainPref3}
        onChange={(e) =>
          setNoDomain({ ...noDomain, domainPref3: e.target.value })
        }
        onClick={() => setNoDomain({ ...noDomain, domainPref3: "www." })}
      />
      <Link
        to={`${
          noDomain.domainPref1 === "" &&
          noDomain.domainPref2 === "" &&
          noDomain.domainPref3 === ""
            ? "/nodomain"
            : "/branding"
        }`}
      >
        <img
          src={arrowBlack}
          className="arrow pointer"
          alt="arrow"
          onClick={() => sendInfo()}
        />
      </Link>
      {preferenceMessage !== "" && (
        <p className="white message">{preferenceMessage}</p>
      )}
    </div>
  );
};

export default NoDomain;

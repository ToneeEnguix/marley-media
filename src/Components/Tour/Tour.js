import React, { useEffect, useState } from "react";
import "./Tour.css";
import "../../App.css";
import arrowBlack from "../../Images/arrowBlack.svg";
import { Link, useLocation } from "react-router-dom";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import TimePicker from "rc-time-picker";
import "rc-time-picker/assets/index.css";
import moment from "moment";

const NoDomain = (props) => {
  const { pathname } = useLocation();
  const [selected, setSelected] = useState(false);
  const [detailsMessage, setDetailsMessage] = useState("");
  const [virtualTour, setVirtualTour] = useState({
    has_tour: true,
    wants_tour: false,
    date_of_shoot: "",
    time_of_shoot: "",
  });

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  useEffect(() => {
    let timer;
    if (detailsMessage !== "") {
      window.clearTimeout(timer);
      timer = window.setTimeout(function () {
        setDetailsMessage("");
      }, 2000);
    }
  }, [detailsMessage]);

  const handleChange = (e) => {
    setVirtualTour({ ...virtualTour, date_of_shoot: e });
  };

  const sendInfo = () => {
    if (virtualTour.wants_tour) {
      if (
        virtualTour.date_of_shoot === "" ||
        virtualTour.time_of_shoot === ""
      ) {
        setDetailsMessage(
          "if you wish to book a virtual tour, please, indicate Date and Time"
        );
      } else {
        props.setInfo(virtualTour);
      }
    } else {
      props.setInfo(virtualTour);
    }
  };

  return (
    <div className="section page">
      <p className="marley bold">marley media</p>
      <p className="subtitle light">Virtual Tour</p>
      <div
        className="checkboxCont"
        onClick={() =>
          setVirtualTour({
            ...virtualTour,
            has_tour: !virtualTour.has_tour,
            wants_tour: !virtualTour.wants_tour,
          })
        }
      >
        <p className="inputAlmost pointer nanum">I have a virtual tour.</p>
        <div className="outside pointer">
          <div className={`inside${virtualTour.has_tour} pointer`}></div>
        </div>
      </div>
      <div
        className="checkboxCont"
        onClick={() =>
          setVirtualTour({
            ...virtualTour,
            wants_tour: !virtualTour.wants_tour,
            has_tour: !virtualTour.has_tour,
          })
        }
      >
        <p className="inputAlmost pointer nanum">
          I want a virtual tour. Add £250.
        </p>
        <div className="outside pointer">
          <div className={`inside${virtualTour.wants_tour} pointer`}></div>
        </div>
      </div>
      <div className="inputContainer">
        {/* <input placeholder="Date of shoot." className="d_input3" /> */}
        <DatePicker
          placeholderText="Date of shoot."
          className={`inputDate pointer ${!virtualTour.wants_tour && "nope"}`}
          readOnly={!virtualTour.wants_tour}
          onCalendarOpen={() => {
            setSelected(true);
            setVirtualTour({ ...virtualTour, date_of_shoot: 1609455600000 });
          }}
          selected={selected && virtualTour.date_of_shoot}
          onChange={(e) => handleChange(e)}
          minDate={moment().toDate()}
        />
        {/* <input
          placeholder="Time of shoot."
          className={`inputDate ${!virtualTour.wants_tour && "nope"}`}
          readOnly={!virtualTour.wants_tour}
          onChange={(e) =>
            setVirtualTour({ ...virtualTour, time_of_shoot: e.target.value })
          }
        /> */}
        <TimePicker
          className={`inputDate ${
            !virtualTour.wants_tour && "nope"
          } time calendar`}
          disabled={!virtualTour.wants_tour}
          showSecond={false}
          onChange={(e) =>
            e
              ? setVirtualTour({ ...virtualTour, time_of_shoot: e._i })
              : setVirtualTour({ ...virtualTour, time_of_shoot: "" })
          }
          minuteStep={15}
        />
      </div>
      <Link
        to={`${
          virtualTour.wants_tour &&
          (virtualTour.date_of_shoot === "" || virtualTour.time_of_shoot === "")
            ? "virtualtour"
            : "/designone"
        }`}
      >
        <img
          src={arrowBlack}
          className="arrow pointer"
          alt="arrow"
          onClick={() => sendInfo()}
        />
      </Link>
      {detailsMessage !== "" && (
        <p className="white message">{detailsMessage}</p>
      )}
    </div>
  );
};

export default NoDomain;

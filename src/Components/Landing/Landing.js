import React, { useEffect } from "react";
import "./landing.css";
import "../../App.css";
import burger from "../../Images/burger.png";
import { Link, useLocation } from "react-router-dom";

const Landing = (props) => {
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  const goTop = () => {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  };

  return (
    <div className="landing page">
      <div className="l_navbar">
        <div className="">
          <p className="marley">marley media</p>
        </div>
        <div className="l_navbarRight">
          <Link className="linkWhite" to="/type">
            <p className="light nanum l_submit pointer">
              Submit your application
            </p>
          </Link>
          <a href="tel:07979054325">
            <p className="thisgray light l_number damascus">079790 54325</p>
          </a>
          <Link to="/menu">
            <img src={burger} className="l_burger pointer" alt="burger" />
          </Link>
        </div>
      </div>
      <section className="l_main">
        <h3 className="l_mainh3 nanum thisgray">Websites that get results</h3>
        <h1 className="l_mainh1 nanumSerif">
          The web design agency for everyday businesses.
        </h1>
        <article className="l_grid2">
          <div className="l_columnLeft">
            <Link className="linkWhite" to="/project">
              <div className="l_website1 pointer">
                <img
                  src="https://res.cloudinary.com/ckellytv/image/upload/v1600080199/mm/Mad-About-Fabrics_cnin4j.png"
                  className="l_blank1 objectfit"
                  alt="project cover"
                />
                <p className="light l_webTitle nanumSerif">Title</p>
                <p className="thisgray light nanum l_description">
                  Description
                </p>
              </div>
            </Link>
            <Link className="linkWhite" to="/project">
              <div className="l_website2 pointer">
                <img
                  src="https://res.cloudinary.com/ckellytv/image/upload/v1600080198/mm/Energise_xuwxsb.png"
                  className="l_blank2 objectfit"
                  alt="project cover"
                />
                <p className="light l_webTitle nanumSerif">Title</p>
                <p className="thisgray light nanum l_description">
                  Description
                </p>
              </div>
            </Link>
            <Link className="linkWhite" to="/project">
              <div className="l_website1 pointer">
                <img
                  src="https://res.cloudinary.com/ckellytv/image/upload/v1600080198/mm/Lynns_lmpo1c.png"
                  className="l_blank1 objectfit"
                  alt="project cover"
                />
                <p className="light l_webTitle nanumSerif">Title</p>
                <p className="thisgray light nanum l_description">
                  Description
                </p>
              </div>
            </Link>
            <Link className="linkWhite" to="/project">
              <div className="l_website3 pointer">
                <img
                  src="https://res.cloudinary.com/ckellytv/image/upload/v1600080198/mm/Upgrade-Fitness-BCN_e9hdxl.png"
                  className="l_blank3 objectfit"
                  alt="project cover"
                />
                <p className="light l_webTitle nanumSerif">Title</p>
                <p className="thisgray light nanum l_description">
                  Description
                </p>
              </div>
            </Link>
          </div>
          <div className="l_columnRight pointer">
            <Link className="linkWhite" to="/project">
              <div className="l_website1">
                <img
                  src="https://res.cloudinary.com/ckellytv/image/upload/v1600080199/mm/Dr-Viks_o4bw1o.png"
                  className="l_blank1 objectfit"
                  alt="project cover"
                />
                <p className="light l_webTitle nanumSerif">Title</p>
                <p className="thisgray light nanum l_description">
                  Description
                </p>
              </div>
            </Link>
            <Link className="linkWhite" to="/project">
              <div className="l_statementContainer">
                <h2 className="nanumSerif light l_statement">
                  This is a big section where we can include some awesome
                  statement that will convince people to trust us with their
                  projects. It is
                </h2>
              </div>
            </Link>
            <Link className="linkWhite" to="/project">
              <div className="l_website1 pointer">
                <img
                  src="https://res.cloudinary.com/ckellytv/image/upload/v1600080301/mm/Regency_ysqgng.png"
                  className="l_blank1 objectfit"
                  alt="project cover"
                />
                <p className="light l_webTitle nanumSerif">Title</p>
                <p className="thisgray light nanum l_description">
                  Description
                </p>
              </div>
            </Link>
            <Link className="linkWhite" to="/project">
              <div className="l_website1 pointer">
                <img
                  src="https://res.cloudinary.com/ckellytv/image/upload/v1600080198/mm/99-East_fvsilw.png"
                  className="l_blank1 objectfit"
                  alt="project cover"
                />
                <p className="light l_webTitle nanumSerif">Title</p>
                <p className="thisgray light nanum l_description">
                  Description
                </p>
              </div>
            </Link>
            <Link className="linkWhite" to="/project">
              <div className="l_website1 pointer">
                <img
                  src="https://res.cloudinary.com/ckellytv/image/upload/v1600080198/mm/Titanic_soiufo.png"
                  className="l_blank1 objectfit"
                  alt="project cover"
                />
                <p className="light l_webTitle nanumSerif">Title</p>
                <p className="thisgray light nanum l_description">
                  Description
                </p>
              </div>
            </Link>
          </div>
        </article>
      </section>
      <footer>
        <p className="light nanum l_reserved">ALL RIGHTS RESERVED, 2020</p>
        <p className="marley pointer" onClick={() => goTop()}>
          marley media
        </p>
      </footer>
    </div>
  );
};

export default Landing;

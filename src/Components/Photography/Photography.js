import React, { useEffect, useState } from "react";
import "./Photography.css";
import "../../App.css";
import arrowBlack from "../../Images/arrowBlack.svg";
import { Link, useLocation } from "react-router-dom";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import TimePicker from "rc-time-picker";
import "rc-time-picker/assets/index.css";
import moment from "moment";

const NoDomain = (props) => {
  const { pathname } = useLocation();
  const [selected, setSelected] = useState(false);
  const [detailsMessage, setDetailsMessage] = useState("");
  const [photography, setPhotography] = useState({
    has_images: true,
    wants_photoshoot: false,
    date_of_shoot: "",
    time_of_shoot: "",
  });

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  useEffect(() => {
    let timer;
    if (detailsMessage !== "") {
      window.clearTimeout(timer);
      timer = window.setTimeout(function () {
        setDetailsMessage("");
      }, 2000);
    }
  }, [detailsMessage]);

  const handleChange = (e) => {
    setPhotography({ ...photography, date_of_shoot: e });
  };

  const sendInfo = () => {
    if (photography.wants_photoshoot) {
      if (
        photography.date_of_shoot === "" ||
        photography.time_of_shoot === ""
      ) {
        setDetailsMessage(
          "if you wish to book a photoshoot, please, indicate Date and Time"
        );
      } else {
        props.setInfo(photography);
      }
    } else {
      props.setInfo(photography);
    }
  };

  return (
    <div className="section page">
      <p className="marley bold">marley media</p>
      <p className="subtitle light">Photography</p>
      <div
        className="checkboxCont"
        onClick={() =>
          setPhotography({
            ...photography,
            has_images: !photography.has_images,
            wants_photoshoot: !photography.wants_photoshoot,
          })
        }
      >
        <p className="inputAlmost pointer nanum">I have my own images.</p>
        <div className="outside pointer">
          <div className={`inside${photography.has_images} pointer`}></div>
        </div>
      </div>
      <div
        className="checkboxCont"
        onClick={() =>
          setPhotography({
            ...photography,
            wants_photoshoot: !photography.wants_photoshoot,
            has_images: !photography.has_images,
          })
        }
      >
        <p className="inputAlmost pointer nanum">
          I wish to book a photoshoot. Add £300.
        </p>
        <div className="outside pointer">
          <div
            className={`inside${photography.wants_photoshoot} pointer`}
          ></div>
        </div>
      </div>
      <div className="inputContainer">
        <DatePicker
          placeholderText="Date of shoot."
          className={`inputDate pointer ${
            !photography.wants_photoshoot && "nope"
          } calendar`}
          readOnly={!photography.wants_photoshoot}
          onCalendarOpen={() => {
            setSelected(true);
            setPhotography({ ...photography, date_of_shoot: 1609455600000 });
          }}
          selected={selected && photography.date_of_shoot}
          onChange={(e) => handleChange(e)}
          minDate={moment().toDate()}
        />
        {/* <input
          placeholder="Time of shoot."
          className={`inputDate ${!photography.wants_photoshoot && "nope"}`}
          readOnly={!photography.wants_photoshoot}
          onChange={(e) =>
            setPhotography({ ...photography, time_of_shoot: e.target.value })
          }
        /> */}
        <TimePicker
          className={`inputDate ${
            !photography.wants_photoshoot && "nope"
          } time`}
          disabled={!photography.wants_photoshoot}
          showSecond={false}
          onChange={(e) =>
            e
              ? setPhotography({ ...photography, time_of_shoot: e._i })
              : setPhotography({ ...photography, time_of_shoot: "" })
          }
          minuteStep={15}
        />
      </div>
      <Link
        to={`${
          photography.wants_photoshoot &&
          (photography.date_of_shoot === "" || photography.time_of_shoot === "")
            ? "photography"
            : "/googlemybusiness"
        }`}
      >
        <img
          src={arrowBlack}
          className="arrow pointer"
          alt="arrow"
          onClick={() => sendInfo()}
        />
      </Link>
      {detailsMessage !== "" && (
        <p className="white message">{detailsMessage}</p>
      )}
    </div>
  );
};

export default NoDomain;

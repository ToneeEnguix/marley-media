import React, { useEffect } from "react";
import "./Service.css";
import "../../App.css";
import { Link, useLocation } from "react-router-dom";

const Service = (props) => {
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return (
    <div className="service page section">
      <p className="marley bold white">marley media</p>
      <p className="light white nanum">Service Agreenment</p>
      <p className="white nanumSerif light s_p">
        This agreement is between ‘you’, the client and ‘us’ ‘we’ Nebula
        Industries Ltd.
      </p>
      <p className="white nanumSerif light s_p">
        ‘You’ understand the terms and conditions as outlined in this service
        agreement. ‘You’ agree to pay a one time fixed fee as stipulated below
        and thereafter initiate a monthly direct debit with ‘us’ for the amount
        of £20.99 for a fixed contract length of 24 months. You acknowledge that
        the final website or application may be as close to the proposed and
        discussed ideas but may not be an exact match given the nature of each
        products custom specifications.
      </p>
      <p className="white nanumSerif light s_p">
        ‘You’ understand that upon presentation of the final website or
        application, ‘you’ have an opportunity to address any changes and beyond
        the initial scope of this specific show, and with those amendments in
        place, any further amendments will be charged at a rate of between
        £15.00 - £150.00 per amendment depending on the scope of changes. If
        ‘you’ have purchased any social media banners or logos ‘you’ accept the
        same policy is in place. You are given one show and then an opportunity
        thereafter to make amendments. Excess charge will be added beyond the
        scope of this show. ‘You’ understand that there is a buy out clause
        should you wish to terminate your subscription early and this will be
        reflected by the cost of the remainder. ‘You’ understand that Nebula
        Industries Ltd. expects to receive a deposit for the amount of 50% of
        total before proceeding with development. The remained of the balance is
        due upon completion at which time you will Receive a request from ‘us’
        to initiate Direct Debit withdrawals.
      </p>
      <div className="s_container">
        <div className="s_containerLil">
          <p className="white s_p2 light">50% Deposit Due</p>
          <p className="white s_p2 light helvetica s_p3">£00.00</p>
        </div>
        <div className="s_containerLil">
          <p className="white s_p2 light">Upon Completion</p>
          <p className="white s_p2 light helvetica s_p3">£00.00</p>
        </div>
        <div className="s_containerLil">
          <p className="white s_p2 light">There After</p>
          <p className="white s_p2 light helvetica s_p3">£20.99 per month</p>
        </div>
        <Link to="/successful">
          <button
            className="s_btn helvetica light pointer"
            onClick={() => props.sendMail(true)}
          >
            I ACCEPT THESE TERMS AND PAY DEPOSIT
          </button>
        </Link>
      </div>
    </div>
  );
};

export default Service;

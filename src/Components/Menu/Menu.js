import React, { useEffect } from "react";
import { Link, useLocation } from "react-router-dom";
import "./menu.css";
import "../../App.css";
import arrowWhite from "../../Images/arrowWhite.png";

const Menu = (props) => {
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return (
    <div className="menu page">
      <Link to="/">
        <img src={arrowWhite} className="menu_arrow pointer" alt="arrow" />
      </Link>
      <div className="m_content">
        <div className="m_grid6">
          <div className="m_column">
            <p className="marley bold">marley media</p>
            <p className="thisgray m_littletext bold">
              We create possibilities
            </p>
            <p className="thisgray m_littletext bold">
              for the connected world.
            </p>
            <p className="bold m_littletext">Be Bold.</p>
          </div>
          <div className="m_column">
            <p className="m_littletitle bold">Explore</p>
            <Link to="/">
              <p className="thisgray m_littletext bold pointer">Home</p>
            </Link>
            <Link to="/about">
              <p className="thisgray m_littletext bold pointer">About</p>
            </Link>
            <p className="thisgray m_littletext bold">Capabilities</p>
            <p className="thisgray m_littletext bold">Careers</p>
          </div>
          <address className="m_column">
            <a
              href="https://www.google.com/maps/place/25+High+St,+Belfast+BT1+1WL,+UK/@54.5995869,-5.9297645,17z/data=!3m1!4b1!4m8!1m2!2m1!1sHigh+St,+Belfast+UK++25,+Highbridge+House!3m4!1s0x486108569d3826eb:0x5debc7fcfe88ed84!8m2!3d54.5995838!4d-5.9275758"
              target="_blank"
              rel="noopener noreferrer"
            >
              <p className="little bold pointer white">Visit Belfast</p>
              <p className="thisgray m_littletext bold pointer">
                High St, Belfast UK
              </p>
              <p className="thisgray m_littletext bold pointer">
                25, Highbridge House
              </p>
            </a>
            <p className="little bold m_business">Contact Belfast</p>
            <a href="mailto:engage@weareenvoy.com">
              <p className="thisgray m_littletext bold pointer">
                engage@weareenvoy.com
              </p>
            </a>
            <a href="tel:9493333106">
              <p className="thisgray m_littletext bold">949.333.3106</p>
            </a>
          </address>
          <address className="m_column">
            <a
              href="https://www.google.com/maps/place/Carrer+de+Balmes,+94,+08008+Barcelona/@41.3916009,2.1578394,17z/data=!3m1!4b1!4m5!3m4!1s0x12a4a29202fdddfd:0xb71dfa785260ce10!8m2!3d41.3915969!4d2.1600281"
              target="_blank"
              rel="noopener noreferrer"
            >
              <p className="little bold white">Visit Barcelona</p>
              <p className="thisgray m_littletext bold">Carrer de Balmes, 94</p>
              <p className="thisgray m_littletext bold">08008 Barcelona</p>
            </a>
            <p className="little bold m_business">Contact Barcelona</p>
            <a href="tel:9493333106">
              <p className="thisgray m_littletext bold">949.333.3106</p>
            </a>
          </address>
          <div className="m_column">
            <p className="little bold">Follow</p>
            <p className="thisgray m_littletext bold">Instagram</p>
            <p className="thisgray m_littletext bold">Twitter</p>
            <p className="thisgray m_littletext bold">LinkedIn</p>
          </div>
          <div className="m_column">
            <p className="little bold">Legal</p>
            <Link to="/legal">
              <p className="thisgray m_littletext bold pointer">Terms</p>
            </Link>
            <Link to="/legal">
              <p className="thisgray m_littletext bold pointer">Privacy</p>
            </Link>
          </div>
        </div>
        <p className="thisgray m_littletext bold">
          © 2020 Envoy. All rights reserved.
        </p>
      </div>
    </div>
  );
};

export default Menu;

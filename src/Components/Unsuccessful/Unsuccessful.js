import React from 'react'
import './Unsuccessful.css'
import "../../App.css";

const Successful = () => {

  return <div className="unsuccessful">
    
    <p className="us_p thisgray nanum light">Error</p>
    <p className="us_p1 white nanumSerif">You have not successfully submitted your application.</p>
    <p className="us_p2 white nanumSerif light">Something went wrong. Please try again</p>

  </div>
}

export default Successful
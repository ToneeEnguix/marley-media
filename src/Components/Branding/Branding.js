import React, { useState, useEffect } from "react";
import "./Branding.css";
import "../../App.css";
import arrowBlack from "../../Images/arrowBlack.svg";
import { Link, useLocation } from "react-router-dom";

const NoDomain = (props) => {
  const { pathname } = useLocation();
  const [branding, setBranding] = useState({
    has_logo: true,
    needs_logo: false,
    has_graphics: true,
    needs_banner: false,
    banners: 0,
    banner_description: "",
  });

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  const sendInfo = () => {
    props.setInfo(branding);
  };

  return (
    <div className="section page">
      <p className="marley bold">marley media</p>
      <p className="subtitle light">Branding</p>
      <div
        className="checkboxCont"
        onClick={() =>
          setBranding({
            ...branding,
            has_logo: !branding.has_logo,
            needs_logo: !branding.needs_logo,
          })
        }
      >
        <p className="inputAlmost pointer nanum">I have my own logo.</p>
        <div className="outside pointer">
          <div className={`inside${branding.has_logo} pointer`}></div>
        </div>
      </div>
      <div
        className="checkboxCont"
        onClick={() =>
          setBranding({
            ...branding,
            needs_logo: !branding.needs_logo,
            has_logo: !branding.has_logo,
          })
        }
      >
        <p className="inputAlmost pointer nanum">I need a logo. Add £50.</p>
        <div className="outside pointer">
          <div className={`inside${branding.needs_logo} pointer`}></div>
        </div>
      </div>
      <div
        className="checkboxCont"
        onClick={() =>
          setBranding({ ...branding, has_graphics: !branding.has_graphics })
        }
      >
        <p className="inputAlmost pointer nanum">I have my own graphics.</p>
        <div className="outside pointer">
          <div className={`inside${branding.has_graphics} pointer`}></div>
        </div>
      </div>
      <div
        className="checkboxCont"
        onClick={() => {
          setBranding({
            ...branding,
            needs_banner: !branding.needs_banner,
            banners: branding.banners === 0 ? 1 : 0,
          });
        }}
      >
        <p className="inputAlmost pointer nanum">
          I need banners for social media. Add £50 per banner.
        </p>
        <div className="outside pointer">
          <div className={`inside${branding.needs_banner} pointer`}></div>
        </div>
      </div>
      <div className="inputContainer">
        <p className={`br_input default nanum ${!branding.needs_banner && "thisgray" }`}>How many would you like?</p>
        <div className="br_btnContainer">
          <button
            className={`br_btn br_btn1 outline ${
              branding.needs_banner && branding.banners > 1
                ? "pointer"
                : "default"
            }`}
            onClick={() =>
              branding.banners > 1 &&
              branding.needs_banner &&
              setBranding({ ...branding, banners: branding.banners - 1 })
            }
          >
            -
          </button>
          <button
            className={`br_btn br_btn2 outline ${
              branding.needs_banner ? "pointer" : "default"
            }`}
            onClick={() =>
              branding.needs_banner &&
              setBranding({ ...branding, banners: branding.banners + 1 })
            }
          >
            +
          </button>
          <div className="br_banner default">{branding.banners}</div>
        </div>
      </div>
      <input
        placeholder="Provide a description of what you would like your social media banners."
        className="inputFull"
        onChange={(e) =>
          setBranding({ ...branding, banner_description: e.target.value })
        }
      />
      <Link to="/photography">
        <img
          src={arrowBlack}
          className="arrow pointer"
          alt="arrow"
          onClick={() => sendInfo()}
        />
      </Link>
    </div>
  );
};

export default NoDomain;

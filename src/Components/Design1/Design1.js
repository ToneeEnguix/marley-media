import React, { useEffect, useState } from "react";
import "./Design1.css";
import "../../App.css";
import arrowBlack from "../../Images/arrowBlack.svg";
import { Link, useLocation } from "react-router-dom";

const NoDomain = (props) => {
  const { pathname } = useLocation();
  const [preferenceMessage, setPreferenceMessage] = useState("");
  const [designOne, setDesignOne] = useState({
    website1: "",
    website2: "",
    website3: "",
  });

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  useEffect(() => {
    let timer;
    if (preferenceMessage !== "") {
      window.clearTimeout(timer);
      timer = window.setTimeout(function () {
        setPreferenceMessage("");
      }, 2000);
    }
  }, [preferenceMessage]);

  const sendInfo = () => {
    if (
      designOne.website1 === "" &&
      designOne.website2 === "" &&
      designOne.website3 === ""
    ) {
      return setPreferenceMessage(
        "Please, write at least one website that you like."
      );
    } else if (checkURL() === false) {
      setInterval(() => {
        setPreferenceMessage("Input must be a URL");
      }, 3000);
    } else {
      props.setInfo(designOne);
    }
  };

  const checkURL = () => {
    const arr = [designOne.website1, designOne.website2, designOne.website3];
    return arr.some((ele) => validURL(ele));
  };

  const validURL = (str) => {
    str = str.toLowerCase();
    const regexp = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
    if (regexp.test(str)) {
      return true;
    } else {
      return false;
    }
  };

  return (
    <div className="section page">
      <p className="marley bold">marley media</p>
      <p className="subtitle light">Design Part I</p>
      <input
        placeholder="Provide the URL for one website you like."
        className="inputFull"
        onChange={(e) =>
          setDesignOne({ ...designOne, website1: e.target.value })
        }
      />
      <input
        placeholder="Provide the URL for another website you like."
        className="inputFull"
        onChange={(e) =>
          setDesignOne({ ...designOne, website2: e.target.value })
        }
      />
      <input
        placeholder="Provide the URL for another website you like."
        className="inputFull"
        onChange={(e) =>
          setDesignOne({ ...designOne, website3: e.target.value })
        }
      />
      <Link
        to={`${
          (designOne.website1 === "" &&
            designOne.website2 === "" &&
            designOne.website3 === "") ||
          checkURL() === false
            ? "/designone"
            : "/designtwo"
        }`}
      >
        <img
          src={arrowBlack}
          className="arrow pointer"
          alt="arrow"
          onClick={() => sendInfo()}
        />
      </Link>
      {preferenceMessage !== "" && (
        <p className="white message">{preferenceMessage}</p>
      )}
    </div>
  );
};

export default NoDomain;

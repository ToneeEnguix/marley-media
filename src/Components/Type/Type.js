import React, { useState, useEffect } from "react";
import "./Type.css";
import "../../App.css";
import arrowBlack from "../../Images/arrowBlack.svg";
import { Link, useLocation } from "react-router-dom";
import check from "../../Images/check.svg";
import _alert from "../../Images/alert.svg";

const Type = (props) => {
  const [selected, setSelected] = useState(1);
  const [checked, setChecked] = useState("");
  const [type, setType] = useState({
    code: "",
    cost: "0500",
  });
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  const checkCode = (e) => {
    e.preventDefault();
    let code = document.getElementById("refInput").value;
    if (code === "050mm") {
      setChecked(<img src={check} alt="check" />);
      setType({ ...type, code });
    } else if (code === "0100mm") {
      setChecked(<img src={check} alt="check" />);
      setType({ ...type, code });
    } else if (code === "0150mm") {
      setChecked(<img src={check} alt="check" />);
      setType({ ...type, code });
    } else {
      setChecked(<img src={_alert} alt="check" />);
    }
  };

  const sendInfo = () => {
    props.setInfo(type);
  };

  return (
    <div className="section page">
      <p className="marley bold">marley media</p>
      <p className="subtitle light">What type of application do you need?</p>
      <form onSubmit={(e) => checkCode(e)}>
        <input
          placeholder="If you have a reference quote type it here."
          className="inputFull"
          onChange={() => setChecked("")}
          id="refInput"
        />
        <button style={{ display: "none" }}></button>
      </form>
      <span className="ap_checked">{checked}</span>
      <div className="ap_container">
        <div
          id="1"
          className={
            selected === 1
              ? "ap_content pointer ap_selected1"
              : "ap_content pointer"
          }
          onClick={() => {
            setSelected(1);
            setType({ ...type, cost: "0500wordpress" });
          }}
        >
          <p className="ap_title">WORDPRESS</p>
          <img
            src="https://seeklogo.com/images/W/wordpress-icon-logo-45667D3313-seeklogo.com.png"
            className="ap_logo1"
            alt="wordpress logo"
          />
          <button className="ap_btn bold ap_btn1">SELECT</button>
        </div>
        <div
          id="5"
          className={
            selected === 2
              ? "ap_content pointer ap_selected2"
              : "ap_content pointer"
          }
          onClick={(e) => {
            e.preventDefault();
            setSelected(2);
            setType({ ...type, cost: "1000ecommerce" });
            return false;
          }}
        >
          <p className="ap_title">E-COMMERCE</p>
          <img
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/WooCommerce_logo.svg/1200px-WooCommerce_logo.svg.png"
            className="ap_logo2"
            alt="woo logo"
          />
          <button className="ap_btn bold ap_btn2">SELECT</button>
        </div>
        <div
          id="3"
          className={
            selected === 3
              ? "ap_content pointer ap_selected3"
              : "ap_content pointer"
          }
          onClick={() => {
            setSelected(3);
            setType({ ...type, cost: "3000custom" });
          }}
        >
          <p className="ap_title">CUSTOM</p>
          <img
            src="https://material-ui.com/static/logo.png"
            className="ap_logo3"
            alt="materials.ui logo"
          />
          <button className="ap_btn bold ap_btn3">SELECT</button>
        </div>
        <div
          id="4"
          className={
            selected === 4
              ? "ap_content pointer ap_selected4"
              : "ap_content pointer"
          }
          onClick={() => {
            setSelected(4);
            setType({ ...type, cost: "6000app" });
          }}
        >
          <p className="ap_title">MOBILE APP</p>
          <img
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/1024px-React-icon.svg.png"
            className="ap_logo4"
            alt="react logo"
          />
          <button className="ap_btn bold ap_btn4">SELECT</button>
        </div>
      </div>
      <Link to="/domainname">
        <img
          src={arrowBlack}
          className="arrow pointer pulse"
          alt="arrow"
          onClick={() => sendInfo()}
        />
      </Link>
    </div>
  );
};

export default Type;

import React, { useEffect, useState } from "react";
import "./Business.css";
import "../../App.css";
import arrowBlack from "../../Images/arrowBlack.svg";
import { Link, useLocation } from "react-router-dom";

const NoDomain = (props) => {
  const { pathname } = useLocation();
  const [business, setBusiness] = useState({
    company_name: "",
    point_of_contact: "",
    business_address: "",
    phone: "",
    email: "",
    social_media: "",
    business_hours: "",
  });

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  const sendInfo = () => {
    props.setInfo(business);
  };

  return (
    <div className="section page">
      <p className="marley bold">marley media</p>
      <p className="subtitle light">Your Business Details</p>
      <div className="inputContainer">
        <input
          placeholder="Company Name"
          className="inputHalf"
          onChange={(e) =>
            setBusiness({ ...business, company_name: e.target.value })
          }
        />
        <input
          placeholder="Point of Contact"
          className="inputHalf"
          onChange={(e) =>
            setBusiness({ ...business, point_of_contact: e.target.value })
          }
        />
      </div>
      <input
        placeholder="Business Address"
        className="inputFull"
        onChange={(e) =>
          setBusiness({ ...business, business_address: e.target.value })
        }
      />
      <div className="inputContainer">
        <input
          placeholder="Phone"
          className="inputHalf"
          onChange={(e) => setBusiness({ ...business, phone: e.target.value })}
        />
        <input
          placeholder="Email"
          className="inputHalf"
          onChange={(e) => setBusiness({ ...business, email: e.target.value })}
        />
      </div>
      <div className="inputContainer">
        <textarea
          placeholder="Social Media"
          className="bs_input nanum"
          onChange={(e) =>
            setBusiness({ ...business, social_media: e.target.value })
          }
        />
        <textarea
          placeholder="Business Hours"
          className="bs_input nanum"
          onChange={(e) =>
            setBusiness({ ...business, business_hours: e.target.value })
          }
        />
      </div>
      <Link to="/final">
        <img
          src={arrowBlack}
          className="arrow pointer"
          alt="arrow"
          onClick={() => sendInfo()}
        />
      </Link>
    </div>
  );
};

export default NoDomain;
